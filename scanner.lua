local args = {...}

local epull = require("event").pull
local wifi = require("component").modem

local mdm = "modem_message"
local timeout = 10
local dev = "nanomachines"
local bcport = 1

local function order(command, ...)
	wifi.broadcast(bcport, dev, command, ...)
	return {epull(timeout, mdm)}
end

local function scan_input(input_index)
	local effect = nil
	order("setInput", input_index, true)
	local responce = order("getActiveEffects")
	if responce[8] ~= "{}" then
		effect = responce[8]
	end
	order("setInput", input_index, false)
	return effect
end

wifi.open(bcport)
order("setResponsePort", bcport)


local total_inputs = math.floor(order("getTotalInputCount")[8])
local effects = {}

for input = 1, total_inputs do
	local effect = scan_input(input)
	if effect then
		table.insert(effects, {input, effect})
	end
end

for i = 1, #effects do print(effects[i][1], effects[i][2]) end

if args[1] == "save" then
	order(false, "saveConfiguration")
end