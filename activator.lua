local epull = require("event").pull
local wifi = require("component").modem
local fs = require("filesystem")
local ps = require("process")
local mdm = "modem_message"
local dev = "nanomachines"
local bcport = 1
local cfg_filename = "nanoconfig"
local cfg_type = ".lua"
local timeout = 10
local workdir = os.getenv().PWD

local function order(command, ...)
	wifi.broadcast(bcport, dev, command, ...)
	return {epull(timeout, mdm)}
end

wifi.open(bcport)
order("setResponsePort",bcport)


if fs.exists(workdir.."/"..cfg_filename..cfg_type) then
	local conf = require(cfg_filename)
	for _, input in pairs(conf) do
		local new_state = not(order("getInput", input)[9])
		order("setInput", input, new_state)
	end
else
	print("File \""..cfg_filename..cfg_type.."\" not found")
end